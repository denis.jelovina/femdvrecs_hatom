# FEMDVRECS_Hatom

Calculation of bound states and  photo ionization cross section from ground state of hydrogen atom by using 
Finite elements and the discrete variable representation (FEM-DVR) radial basis and exterior complex scaling (ECS). 
Using linearly polarized laser field in z-directoin. Solving time dependent Schrödinger equation using Crank-Nicolson propagator, 
either lenght or velocity gauge. The example laser field is prvided in **fieldtest** directory, it contains time, electric field and vector potential in *Field.txt*. 
The Fourier transforms are given in *Fourier.txt* file. Fields are calualted by https://github.com/jfeist/laserfields

To be added: angular distributions of ejected electron.